<?php
/**
 * Meta Boxes
 *
 * @package kale
 */
?>
<?php

define('THEME_PATH', get_template_directory());
add_action('admin_init', 'kale_meta_init');
add_action( 'load-post.php', 'kale_meta_init' );
add_action( 'load-post-new.php', 'kale_meta_init' );

 
function kale_meta_init(){
    if(is_admin()) 
        wp_enqueue_style('kale-meta-boxes', get_template_directory_uri() . '/meta_boxes/style.css');
    if(array_key_exists('post_ID', $_POST)) $post_id = $_POST['post_ID'] ;
    else if (array_key_exists('post', $_GET)) $post_id = $_GET['post'];
    else $post_id = '';
    
    add_action( 'add_meta_boxes', 'kale_page_options_meta_box_register' );
    add_action( 'save_post', 'kale_page_options_meta_box_save' );
}

/*** Page Options ***/

function kale_page_options_meta_box_register() {
	add_meta_box(
		'kale_page_options_meta_box',
		esc_html__( 'Kale - Page Options', 'kale' ),
		'kale_page_options_meta_box_render',
		'page',
		'side',
		'high'
	);
}

function kale_page_options_meta_box_render( $post ) {
	global $post;
	$meta = get_post_meta($post->ID,'_page_options_meta',TRUE);
    include(get_template_directory() . '/meta_boxes/meta_box_page.php');
    wp_nonce_field( __FILE__, 'kale_page_options_nonce' ); 
}
    
function kale_page_options_meta_box_save( $post_id ) {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	if ( ! isset( $_POST['kale_page_options_nonce'] ) ) {
		return;
	}

	if ( ! wp_verify_nonce( $_POST['kale_page_options_nonce'], __FILE__ ) ) {
		return;
	}

	$meta       = $_POST['_page_options_meta'];
	update_post_meta( $post_id, '_page_options_meta', $meta );
}


/*** Helper ***/

function kale_meta_clean(&$arr){
    if (is_array($arr)){
        foreach ($arr as $i => $v){
            if (is_array($arr[$i])) {
                kale_meta_clean($arr[$i]);
                if (!count($arr[$i])) 
                    unset($arr[$i]);
            }
            else{
                if (trim($arr[$i]) == '') 
                    unset($arr[$i]);
            }
        }
        if (!count($arr)) 
			$arr = NULL;
    }
}

?>