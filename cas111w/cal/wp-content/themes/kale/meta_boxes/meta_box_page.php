<div class="meta_control">

<label><strong><?php _e('Featured Image Layout', 'kale'); ?></strong></label>

<p><label><input type="radio" name="_page_options_meta[featured_image]" id="featured_image_default" value="Default" <?php if(!empty($meta)) { if($meta['featured_image']=='Default' || empty($meta['featured_image'])) echo 'checked'; } else echo 'checked'; ?>> <?php _e('Default', 'kale') ?></label><br /><small><?php _e('Show featured image under the title, in the content area', 'kale') ?>.</small></p>

<p><label><input type="radio" name="_page_options_meta[featured_image]" id="featured_image_banner" value="Banner" <?php if(!empty($meta)) if($meta['featured_image']=='Banner') echo 'checked'; ?>> <?php _e('Banner', 'kale') ?></label><br /><small><?php _e('Show featured image at the top, like a banner.', 'kale') ?></small></p>

<p><label><input type="radio" name="_page_options_meta[featured_image]" id="featured_image_hide" value="Hide" <?php if(!empty($meta)) if($meta['featured_image']=='Hide') echo 'checked'; ?>> <?php _e('Hide', 'kale') ?></label><br /><small><?php _e('Do not show the featured image.', 'kale') ?></small></p>

</div>

