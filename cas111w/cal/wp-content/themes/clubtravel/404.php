<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage ClubTravel
 */

get_header(); ?>

<section class="content content-404">
	<div class="container">
		<div class="row">
		<?php if ( is_active_sidebar( 'primary_sidebar' ) ) : ?>
			<div class="col-xs-12 col-sm-8">
		<?php else : ?>
			<div class="col-xs-12">
		<?php endif; ?>
				<div class="row">
					<div class="col-xs-12">
						<div class="page-title">
							<h1><?php esc_html_e( 'The Page You Are Looking For Doesn&rsquo;t Exist.', 'clubtravel' ); ?></h1>
						</div>
						<h2><?php esc_html_e( 'We are very sorry for the inconvenience.', 'clubtravel' ); ?></h2>
						<p><?php esc_html_e( 'Perhaps, Try using the search box below.', 'clubtravel' ); ?></p>
						<?php get_search_form(); ?>
					</div>
				</div>
			</div>
		<?php if ( is_active_sidebar( 'primary_sidebar' ) ) : ?>
			<div class="col-xs-12 col-sm-4">
				<?php get_sidebar(); ?>
			</div>
		<?php endif; ?>
		</div>
	</div>
</section>

<?php get_footer(); ?>
