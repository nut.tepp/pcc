<?php
/**
 * The template for displaying all the posts
 *
 * @package WordPress
 * @subpackage ClubTravel
 */

?>
<div class="posts">
	<?php $i = 1; ?>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php if ( has_post_thumbnail() ) : ?>
		<div class="post-item post-item-<?php echo esc_attr( $i ); ?>">
		<?php else : ?>
		<div class="post-item post-item-<?php echo esc_attr( $i ); ?> post-item-no-image">
		<?php endif; ?>
			<div class="row">
			<?php if ( has_post_thumbnail() ) : ?>
				<?php if ( 1 === $i ) : ?>
					<div class="col-xs-12">
				<?php else : ?>
					<div class="col-xs-12 col-md-6 col-lg-7">
				<?php endif; ?>
					<?php $backgroundimg = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium_large' ); ?>
					<div class="post-image" style="background-image: url('<?php echo esc_url( $backgroundimg[0] ); ?>');">
						<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"></a>
					</div>
				</div>
			<?php endif; ?>
			<?php if ( 1 === $i ) : ?>
				<div class="col-xs-12">
			<?php else : ?>
				<?php if ( has_post_thumbnail() ) : ?>
					<div class="col-xs-12 col-md-6 col-lg-5">
				<?php else : ?>
					<div class="col-xs-12">
				<?php endif; ?>
			<?php endif; ?>
					<div class="post-info">
						<div class="post-categories-list">
							<?php echo wp_kses_post( get_the_category_list() ); ?>
						</div>
						<div class="post-title">
							<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
						</div>
						<?php get_template_part( 'inc/post', 'meta' ); ?>
						<div class="post-message">
							<?php the_excerpt(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
	if ( isset( $i ) && 2 === $i ) {
		$i = 0;
	}
	?>
	<?php $i++; ?>
	<?php endwhile; ?>
	<?php else : ?>
	<?php get_template_part( 'no-results' ); ?>
	<?php endif; ?>
</div>
<div class="page-nav">
	<?php the_posts_pagination(); ?>
</div>
