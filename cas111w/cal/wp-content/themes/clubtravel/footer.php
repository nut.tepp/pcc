<?php
/**
 * The template for displaying the footer
 *
 * @package WordPress
 * @subpackage ClubTravel
 */

?>
<footer>
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="footer-main">
					<div class="row">
						<div class="col-xs-12 text-center">
							<p><?php esc_html_e( 'Theme by', 'clubtravel' ); ?> <?php if ( is_home( ) && ! is_paged( ) ) : ?><a href="<?php echo esc_url( __( 'https://www.thewpclub.com', 'clubtravel' ) ); ?>" title="The WP Club" target="_blank"><?php endif; ?>The WP Club<?php if ( is_home( ) && ! is_paged( ) ) : ?></a><?php endif; ?>. <span class="sep"> | </span> <a href="<?php echo esc_url( __( 'https://wordpress.org/', 'clubtravel' ) ); ?>" target="_blank"><?php printf( esc_attr( 'Proudly powered by %s', 'clubtravel' ), 'WordPress' ); ?></a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
