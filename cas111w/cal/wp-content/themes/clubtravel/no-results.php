<?php
/**
 * The template for displaying when there is no result
 *
 * @package WordPress
 * @subpackage ClubTravel
 */

?>
<h2><?php esc_html_e( 'Oops..! No Results Found.', 'clubtravel' ); ?></h2>
<p><?php esc_html_e( 'Perhaps, Try searching with different keywords.', 'clubtravel' ); ?></p>
<?php get_search_form(); ?>
