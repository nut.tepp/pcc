<?php
/**
 * ClubTravel functions and definitions
 *
 * @package WordPress
 * @subpackage ClubTravel
 */

/**
 * ClubTravel Theme Setup
 */
function clubtravel_setup() {
	// Meta Title.
	add_theme_support( 'title-tag' );

	// Automatic Feed Links.
	add_theme_support( 'automatic-feed-links' );

	// Logo Support.
	add_theme_support( 'custom-logo', array(
		'width'       => 215,
		'height'      => 38,
		'flex-width'  => true,
		'flex-height' => true,
		'header-text' => array( 'site-title', 'site-description' ),
	) );

	// Featured Image.
	add_theme_support( 'post-thumbnails' );

	// Language Support.
	load_theme_textdomain( 'clubtravel', get_template_directory() . '/languages' );

	// Header Image.
	$clubtravel_args = array(
		'flex-width'    		=> true,
		'width'         		=> 1500,
		'flex-height'   		=> true,
		'height'        		=> 900,
		'default-text-color'	=> '#333333',
	);
	add_theme_support( 'custom-header', $clubtravel_args );

	// Content Width.
	if ( ! isset( $content_width ) ) {
		$content_width = 1170;
	}
}
add_action( 'after_setup_theme', 'clubtravel_setup' );

/**
 * Color / Social Customizer
 *
 * @param array $clubtravel_wp_customize Theme Colors & Social Media.
 */
function clubtravel_customize_register( $clubtravel_wp_customize ) {
	$clubtravel_colors   	= array();
	$clubtravel_colors[] 	= array(
		'slug' => 'default_color',
		'default' => '#32ccd2',
		'label' => __( 'Default Color ', 'clubtravel' ),
	);

	foreach ( $clubtravel_colors as $clubtravel_color ) {
		$clubtravel_wp_customize->add_setting( $clubtravel_color['slug'], array(
			'default' => $clubtravel_color['default'],
			'type' => 'theme_mod',
			'capability' => 'edit_theme_options',
			'sanitize_callback' => 'sanitize_hex_color',
		) );
		$clubtravel_wp_customize->add_control( new WP_Customize_Color_Control( $clubtravel_wp_customize, $clubtravel_color['slug'], array(
			'label' => $clubtravel_color['label'],
			'section' => 'colors',
			'settings' => $clubtravel_color['slug'],
		) ) );
	}

	$clubtravel_wp_customize->add_section( 'sociallinks', array(
		'title' => __( 'Social Links','clubtravel' ),
		'description' => __( 'Add Your Social Links Here.','clubtravel' ),
		'priority' => '900',
	) );

	$clubtravel_wp_customize->add_setting( 'clubtravel_facebooklink', array(
		'default' => '',
		'sanitize_callback' => 'esc_url_raw',
	) );
	$clubtravel_wp_customize->add_control( 'clubtravel_facebooklink', array(
		'label' => __( 'Facebook URL', 'clubtravel' ),
		'section' => 'sociallinks',
	) );
	$clubtravel_wp_customize->add_setting( 'clubtravel_twitterlink', array(
		'default' => '',
		'sanitize_callback' => 'esc_url_raw',
	) );
	$clubtravel_wp_customize->add_control( 'clubtravel_twitterlink', array(
		'label' => __( 'Twitter URL', 'clubtravel' ),
		'section' => 'sociallinks',
	) );
	$clubtravel_wp_customize->add_setting( 'clubtravel_pinterestlink', array(
		'default' => '',
		'sanitize_callback' => 'esc_url_raw',
	) );
	$clubtravel_wp_customize->add_control( 'clubtravel_pinterestlink', array(
		'label' => __( 'Pinterst URL', 'clubtravel' ),
		'section' => 'sociallinks',
	) );
	$clubtravel_wp_customize->add_setting( 'clubtravel_instagramlink', array(
		'default' => '',
		'sanitize_callback' => 'esc_url_raw',
	) );
	$clubtravel_wp_customize->add_control( 'clubtravel_instagramlink', array(
		'label' => __( 'Instagram URL', 'clubtravel' ),
		'section' => 'sociallinks',
	) );
	$clubtravel_wp_customize->add_setting( 'clubtravel_linkedinlink', array(
		'default' => '',
		'sanitize_callback' => 'esc_url_raw',
	) );
	$clubtravel_wp_customize->add_control( 'clubtravel_linkedinlink', array(
		'label' => __( 'LinkedIn URL', 'clubtravel' ),
		'section' => 'sociallinks',
	) );
	$clubtravel_wp_customize->add_setting( 'clubtravel_googlepluslink', array(
		'default' => '',
		'sanitize_callback' => 'esc_url_raw',
	) );
	$clubtravel_wp_customize->add_control( 'clubtravel_googlepluslink', array(
		'label' => __( 'Google+ URL', 'clubtravel' ),
		'section' => 'sociallinks',
	) );
	$clubtravel_wp_customize->add_setting( 'clubtravel_youtubelink', array(
		'default' => '',
		'sanitize_callback' => 'esc_url_raw',
	) );
	$clubtravel_wp_customize->add_control( 'clubtravel_youtubelink', array(
		'label' => __( 'YouTube URL', 'clubtravel' ),
		'section' => 'sociallinks',
	) );
	$clubtravel_wp_customize->add_setting( 'clubtravel_vimeo', array(
		'default' => '',
		'sanitize_callback' => 'esc_url_raw',
	) );
	$clubtravel_wp_customize->add_control( 'clubtravel_vimeo', array(
		'label' => __( 'Vimeo URL', 'clubtravel' ),
		'section' => 'sociallinks',
	) );
	$clubtravel_wp_customize->add_setting( 'clubtravel_tumblrlink', array(
		'default' => '',
		'sanitize_callback' => 'esc_url_raw',
	) );
	$clubtravel_wp_customize->add_control( 'clubtravel_tumblrlink', array(
		'label' => __( 'Tumblr URL', 'clubtravel' ),
		'section' => 'sociallinks',
	) );
	$clubtravel_wp_customize->add_setting( 'clubtravel_flickrlink', array(
		'default' => '',
		'sanitize_callback' => 'esc_url_raw',
	) );
	$clubtravel_wp_customize->add_control( 'clubtravel_flickrlink', array(
		'label' => __( 'Flickr URL', 'clubtravel' ),
		'section' => 'sociallinks',
	) );
}
add_action( 'customize_register', 'clubtravel_customize_register' );

/**
 * Bootstrap 3.3.7
 */
function clubtravel_bootstrap() {
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.css' );
	wp_enqueue_style( 'fontawesome', get_template_directory_uri() . '/css/font-awesome.css' );
	wp_enqueue_style( 'clubtravel-googlefonts', clubtravel_google_fonts_url(), array(), null );
	wp_enqueue_style( 'clubtravel-style', get_stylesheet_uri() );
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.js', array( 'jquery' ), '' ,true );
	wp_enqueue_script( 'clubtravel-script', get_template_directory_uri() . '/js/script.js', array( 'jquery' ), '', true );
}
add_action( 'wp_enqueue_scripts', 'clubtravel_bootstrap' );

/**
 * Google Font
 */
function clubtravel_google_fonts_url() {
	$font_families = array( 'Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i' );
	$query_args = array(
		'family' => rawurlencode( implode( '|', $font_families ) ),
		'subset' => rawurlencode( 'latin,latin-ext' ),
	);
	$fonts_url = add_query_arg( $query_args, '//fonts.googleapis.com/css' );
	return apply_filters( 'clubtravel_google_fonts_url', $fonts_url );
}

/**
 * Navigation
 */
function clubtravel_register_menu() {
	register_nav_menus( array(
		'primary' => esc_html__( 'Top Primary Menu', 'clubtravel' ),
	) );
}
add_action( 'init', 'clubtravel_register_menu' );

/**
 * Bootstrap Navigation
 */
function clubtravel_bootstrap_menu() {
	wp_nav_menu( array(
		'theme_location'    => 'primary',
		'depth'             => 2,
		'menu_class'        => 'nav navbar-nav pull-right',
		'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
		'walker'            => new WP_Bootstrap_Navwalker(),
	) );
}
require get_template_directory() . '/inc/class-wp-bootstrap-navwalker.php';

/**
 * Header Style
 */
function clubtravel_header_style() {
	if ( ! display_header_text() ) {
		echo'
		<style type="text/css">
			.site-title,
			.site-description {
				position: absolute;
				clip: rect(1px 1px 1px 1px);
				clip: rect(1px, 1px, 1px, 1px);
			}
		</style>
		';
	}
}
add_action( 'wp_head', 'clubtravel_header_style' );

/**
 * Custom Colors
 */
function clubtravel_customizer_css() {
	$clubtravel_default_color 		= get_theme_mod( 'default_color' );
	$clubtravel_header_text_color 	= get_header_textcolor();
	?>
	<style type="text/css">
		.btn-search,
		.btn-submit,
		.btn-search:hover,
		.btn-search:focus,
		.btn-submit:hover,
		.btn-submit:focus,
		.navbar-default .navbar-toggle,
		.navbar-default .navbar-toggle:hover,
		.navbar-default .navbar-toggle:focus,
		.site-header,
		.post-item .post-info .post-categories-list .post-categories li a,
		.post-item .post-info .post-message .more-link,
		.post-item .post-info .post-tags .tags a,
		.nav-links .page-numbers.current,
		.comment .reply a,
		.tooltip-inner {
			background-color: <?php echo esc_attr( $clubtravel_default_color ); ?>;
		}

		.content:before,
		.content:after,
		.post-item .post-image:after,
		.comment.depth-1 {
			border-color: <?php echo esc_attr( $clubtravel_default_color ); ?>;
		}

		.tooltip.left .tooltip-arrow {
			border-left-color: <?php echo esc_attr( $clubtravel_default_color ); ?>;
		}

		.tooltip.top .tooltip-arrow {
			border-top-color: <?php echo esc_attr( $clubtravel_default_color ); ?>;
		}

		a:hover,
		a:focus,
		.navbar-default .navbar-brand:hover,
		.navbar-default .navbar-brand:focus,
		.navbar-default .navbar-nav > li > a:hover,
		.navbar-default .navbar-nav > li > a:focus,
		.navbar-default .navbar-nav > .active > a,
		.navbar-default .navbar-nav > .active > a:hover,
		.navbar-default .navbar-nav > .active > a:focus,
		.navbar-default .navbar-nav > .open > a,
		.navbar-default .navbar-nav > .open > a:hover,
		.navbar-default .navbar-nav > .open > a:focus,
		.navbar-default .navbar-nav > li > .dropdown-menu > li > a:hover,
		.navbar-default .navbar-nav > li > .dropdown-menu > li > a:focus,
		.navbar-default .navbar-nav > li > .dropdown-menu > .active > a,
		.navbar-default .navbar-nav > li > .dropdown-menu > .active > a:hover,
		.navbar-default .navbar-nav > li > .dropdown-menu > .active > a:focus,
		footer a {
			color: <?php echo esc_attr( $clubtravel_default_color ); ?>;
		}

		.navbar-default .navbar-brand {
			color: #<?php echo esc_attr( $clubtravel_header_text_color ); ?>;
		}
	</style>
	<?php
}
add_action( 'wp_head', 'clubtravel_customizer_css' );

/**
 * Widgets
 */
function clubtravel_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Primary Sidebar', 'clubtravel' ),
		'id'            => 'primary_sidebar',
		'before_widget' => '<div class="sidebar-item">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Header', 'clubtravel' ),
		'id'            => 'header-sidebar',
		'before_widget' => '<div class="header">',
		'after_widget'  => '</div>',
		'before_title'  => '<h1>',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'clubtravel_widgets_init' );

/**
 * Post Read More
 *
 * @param array $link Show more link.
 */
function clubtravel_excerpt_more( $link ) {
	if ( is_admin() ) {
		return $link;
	}

	$link = sprintf( '<p class="link-more"><a href="%1$s" class="more-link">%2$s</a></p>',
		esc_url( get_permalink( get_the_ID() ) ),
		// translators: %s containing the title of the post or page.
		sprintf( __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'clubtravel' ), get_the_title( get_the_ID() ) )
	);
	return ' &hellip; ' . $link;
}
add_filter( 'excerpt_more', 'clubtravel_excerpt_more' );

/**
 * Bootstrap Images img-responsive
 *
 * @param array $html Html code for image with Bootstrap class.
 */
function clubtravel_bootstrap_responsive_images( $html ) {
	$classes = 'img-responsive';
	if ( preg_match( '/<img.*? class="/', $html ) ) {
		$html = preg_replace( '/(<img.*? class=".*?)(".*?\/>)/', '$1 ' . $classes . ' $2', $html );
	} else {
		$html = preg_replace( '/(<img.*?)(\/>)/', '$1 class="' . $classes . '" $2', $html );
	}
	return $html;
}
add_filter( 'the_content', 'clubtravel_bootstrap_responsive_images', 10 );
add_filter( 'post_thumbnail_html', 'clubtravel_bootstrap_responsive_images', 10 );

/**
 * Comment Reply
 */
function clubtravel_comment_reply() {
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'clubtravel_comment_reply' );

/**
 * Bootstrap Comment Form
 *
 * @param array $clubtravel_fields Comment Form Fields.
 */
function clubtravel_comment_form_fields( $clubtravel_fields ) {
	$clubtravel_commenter = wp_get_current_commenter();
	$clubtravel_req      = get_option( 'require_name_email' );
	$clubtravel_aria_req = ( $clubtravel_req ? " aria-required='true'" : '' );
	$clubtravel_html5    = current_theme_supports( 'html5', 'comment-form' ) ? 1 : 0;
	$clubtravel_fields   = array(
		'author' 	=> '<div class="form-group comment-form-author"><input class="form-control" id="author" name="author" type="text" value="' . esc_attr( $clubtravel_commenter['comment_author'] ) . '" placeholder="' . ( $clubtravel_req ? '* ' : '' ) . __( 'Name', 'clubtravel' ) . '..." size="30"' . $clubtravel_aria_req . ' /></div>',
		'email'  	=> '<div class="form-group comment-form-email"><input class="form-control" id="email" name="email" ' . ( $clubtravel_html5 ? 'type="email"' : 'type="text"' ) . ' value="' . esc_attr( $clubtravel_commenter['comment_author_email'] ) . '" placeholder="' . ( $clubtravel_req ? '* ' : '' ) . __( 'Email', 'clubtravel' ) . '..." size="30"' . $clubtravel_aria_req . ' /></div>',
		'url'    	=> '<div class="form-group comment-form-url"><input class="form-control" id="url" name="url" ' . ( $clubtravel_html5 ? 'type="url"' : 'type="text"' ) . ' value="' . esc_attr( $clubtravel_commenter['comment_author_url'] ) . '" placeholder="' . __( 'Website', 'clubtravel' ) . '..." size="30" /></div>',
	);
	return $clubtravel_fields;
}
add_filter( 'comment_form_default_fields', 'clubtravel_comment_form_fields' );

/**
 * Bootstrap Comment Form
 *
 * @param array $clubtravel_args Comment Form Fields.
 */
function clubtravel_comment_form( $clubtravel_args ) {
	$clubtravel_args['comment_field'] = '<div class="form-group comment-form-comment"><textarea class="form-control" id="comment" name="comment" cols="45" rows="8" placeholder="' . esc_attr__( 'Comment', 'clubtravel' ) . '..." aria-required="true"></textarea></div>';
	$clubtravel_args['class_submit'] = 'btn btn-default btn-submit';
	return $clubtravel_args;
}
add_filter( 'comment_form_defaults', 'clubtravel_comment_form' );
?>
