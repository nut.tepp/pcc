<?php
/**
 * The header for our theme including the navigation and the photo header
 *
 * @package WordPress
 * @subpackage ClubTravel
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="profile" href="http://gmpg.org/xfn/11">
<?php if ( is_singular() && pings_open() ) : ?>
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php endif; ?>
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<nav class="navbar navbar-default">
	<div class="container">
		<div class="navbar-header">
			<?php if ( has_nav_menu( 'primary' ) ) { ?>
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only"><?php esc_html_e( 'Toggle navigation', 'clubtravel' ); ?></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<?php } ?>
			<a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
				<?php
				$clubtravel_custom_logo_id = get_theme_mod( 'custom_logo' );
				$clubtravel_logo = wp_get_attachment_image_src( $clubtravel_custom_logo_id , 'full' );
				if ( has_custom_logo() ) {
				?>
				<img src="<?php echo esc_url( $clubtravel_logo[0] ); ?>" class="img-responsive">
				<?php } else { ?>
				<span class="site-title"><?php bloginfo( 'name' ); ?></span>
				<?php $clubtravel_description = get_bloginfo( 'description', 'display' ); ?>
				<?php if ( $clubtravel_description || is_customize_preview() ) : ?>
					<span class="site-description"><?php echo esc_attr( $clubtravel_description ); ?></span>
				<?php endif; ?>
				<?php } ?>
			</a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
			<?php clubtravel_bootstrap_menu(); ?>
		</div>
	</div>
</nav>

<?php if ( is_front_page() || is_home() ) : ?>
	<?php if ( is_active_sidebar( 'header-sidebar' ) ) : ?>
		<?php if ( get_header_image() ) : ?>
			<div class="site-header site-header-image" style="background-image:url('<?php header_image(); ?>');">
		<?php else : ?>
			<div class="site-header site-header-image">
		<?php endif; ?>
	<?php else : ?>
		<?php if ( get_header_image() ) : ?>
			<div class="site-header" style="background-image:url('<?php header_image(); ?>');">
		<?php else : ?>
			<div class="site-header">
		<?php endif; ?>
	<?php endif; ?>
<?php else : ?>
	<?php if ( get_header_image() ) : ?>
		<div class="site-header" style="background-image:url('<?php header_image(); ?>');">
	<?php else : ?>
		<div class="site-header">
	<?php endif; ?>
<?php endif; ?>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-8 col-sm-offset-2 text-center">
				<div class="header-info">
					<?php if ( is_front_page() || is_home() ) : ?>
						<?php if ( is_active_sidebar( 'header-sidebar' ) ) : ?>
							<?php dynamic_sidebar( 'header-sidebar' ); ?>
						<?php endif; ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	<?php if ( is_front_page() || is_home() ) : ?>
		<?php get_template_part( 'inc/social' ); ?>
	<?php endif; ?>
</div>
