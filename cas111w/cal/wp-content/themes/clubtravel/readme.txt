=== ClubTravel ===
Contributors: ClubWP
Tags: custom-menu, custom-colors, custom-header, sticky-post, featured-images, translation-ready, threaded-comments, blog
Requires at least: 4.3
Tested up to: 4.8
Stable tag: 4.8
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
 
== Description ==
 
A Free WordPress Blog / Magazine / Personal / Writers Theme. ClubTravel is a clean minimal and responsive WordPress theme well suited for fashion, writers, travel, health, business, finance, portfolio, design, art, photography, personal or any other creative websites and blogs. Developed with bootstrap framework that makes it mobile and tablet friendly. ClubTravel has the option to add your social media links. Get free support at https://ask.thewpclub.com/

Theme Link: https://www.thewpclub.com/clubtravel-wordpress-theme/
Demo Link : https://demo.thewpclub.com/clubtravel/

== Copyright ==
 
ClubTravel WordPress Blog Theme, Copyright 2017 thewpclub.com

ClubTravel is licensed under GNU General Public License V2 or later.

ClubTravel bundles the following third-party resources:

###
Framework - Bootstrap
ClubTravel is based on Bootstrap.
Resource URI: http://getbootstrap.com/
Licensed Under MIT( https://opensource.org/licenses/MIT)
###

###
Google Fonts.
Resource URI: http://www.google.com/fonts/
Licensed Under: SIL Open Font License, 1.1 (http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL)
###

###
FontAwesome 4.7.5
Copyright 2012 Dave Gandy
Font License: SIL OFL 1.1
Code License: MIT License
http://fontawesome.io/license/
###

###
WP-Bootstrap-NavWalker
Resource URI: https://github.com/wp-bootstrap
Licensed Under: GPLv2 license (http://www.gnu.org/licenses/gpl-2.0.html)
###

###
Images used in the demo are free for personal and commercial use.
Resource URI: https://www.pexels.com/
Licensed under the terms of CC0

Screenshot images
- https://www.pexels.com/photo/road-landscape-nature-woman-126313/
- https://www.pexels.com/photo/woman-girl-sea-sky-31270/
###

== Installation ==
 
1. Upload the zip file to your themes directory.
2. Apply the theme.
3. Go to Appearance > Customize in main menu and configure your theme's settings.
 
== Frequently Asked Questions ==
 
If you have any question you can ask it at https://ask.thewpclub.com/
 
== Screenshots ==
 
1. screenshot.png
 
== Changelog ==

### 1.0.7 - 07/19/2017

Changes:

- Bug fixes


### 1.0.6 - 07/10/2017

Changes:

- Bug fixes


### 1.0.5 - 07/08/2017

Changes:

- Change readme file
- Bug fixes


### 1.0.4 - 07/08/2017

Changes:

- Bug fixes


### 1.0.3 - 07/07/2017

Changes:

- Fix some style points I got from a user.


### 1.0.2 - 07/04/2017

Changes:

- Some php code changes and fixes


### 1.0.1 - 06/13/2017

Changes:

- Some php code changes and fixes


### 1.0.0 - 05/05/2017

Changes:

* INITIAL RELEASE *
