<?php
/**
 * The template for displaying all pages
 *
 * @package WordPress
 * @subpackage ClubTravel
 */

get_header(); ?>

<section class="content content-page">
	<div class="container">
		<div class="row">
		<?php if ( is_active_sidebar( 'primary_sidebar' ) ) : ?>
			<div class="col-xs-12 col-sm-8">
		<?php else : ?>
			<div class="col-xs-12">
		<?php endif; ?>
				<div class="row">
					<div class="col-xs-12">
				<?php if ( have_posts() ) : ?>
					<?php while ( have_posts() ) : the_post(); ?>	
						<div class="page-title">
							<h1><?php the_title(); ?></h1>
						</div>
						<div class="page-message">
							<?php the_content(); ?>
							<?php wp_link_pages( array(
								'before' => '<div class="page-link">' . __( 'Pages:', 'clubtravel' ),
								'after' => '</div>',
							) ); ?>
						</div>
						<div class="page-edit clearfix">
							<?php edit_post_link( __( 'Edit', 'clubtravel' ), '<span class="edit-link">', '</span>' ); ?>
						</div>
						<?php if ( comments_open() || '0' !== get_comments_number() ) : ?>
						<?php comments_template( '', true ); ?>
						<?php endif; ?>
					<?php endwhile; ?>
				<?php endif; ?>
					</div>
				</div>
			</div>
		<?php if ( is_active_sidebar( 'primary_sidebar' ) ) : ?>
			<div class="col-xs-12 col-sm-4">
				<?php get_sidebar(); ?>
			</div>
		<?php endif; ?>
		</div>
	</div>
</section>

<?php get_footer(); ?>
