<?php
/**
 * The template for displaying archive pages
 *
 * @package WordPress
 * @subpackage ClubTravel
 */

get_header(); ?>

<section class="content content-archive">
	<div class="container">
		<div class="row">
		<?php if ( is_active_sidebar( 'primary_sidebar' ) ) : ?>
			<div class="col-xs-12 col-sm-8">
		<?php else : ?>
			<div class="col-xs-12">
		<?php endif; ?>
				<div class="row">
					<div class="col-xs-12">
						<div class="page-title">
							<h1><?php the_archive_title(); ?></h1>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<?php get_template_part( 'loop' ); ?>
					</div>
				</div>
			</div>
		<?php if ( is_active_sidebar( 'primary_sidebar' ) ) : ?>
			<div class="col-xs-12 col-sm-4">
				<?php get_sidebar(); ?>
			</div>
		<?php endif; ?>
		</div>
	</div>
</section>

<?php get_footer(); ?>
