<?php
/**
 * The template for displaying social media icons
 *
 * @package WordPress
 * @subpackage ClubTravel
 */

?>
<?php if ( get_theme_mod( 'clubtravel_facebooklink' ) || get_theme_mod( 'clubtravel_twitterlink' ) || get_theme_mod( 'clubtravel_pinterestlink' ) || get_theme_mod( 'clubtravel_instagramlink' ) || get_theme_mod( 'clubtravel_linkedinlink' ) || get_theme_mod( 'clubtravel_googlepluslink' ) || get_theme_mod( 'clubtravel_youtubelink' ) || get_theme_mod( 'clubtravel_vimeo' ) || get_theme_mod( 'clubtravel_tumblrlink' ) || get_theme_mod( 'clubtravel_flickrlink' ) ) : ?>
<div class="social-icons">
	<ul class="list-unstyled list-social-icons">
	<?php endif; ?>
	<?php if ( get_theme_mod( 'clubtravel_facebooklink' ) ) : ?>
		<li><a href="<?php echo esc_url( get_theme_mod( 'clubtravel_facebooklink' ) ); ?>" data-toggle="tooltip" data-placement="<?php if ( is_active_sidebar( 'header-sidebar' ) ) : ?>left<?php else : ?>top<?php endif; ?>" title="<?php esc_html_e( 'Facebook', 'clubtravel' ); ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
	<?php endif; ?>
	<?php if ( get_theme_mod( 'clubtravel_twitterlink' ) ) : ?>
		<li><a href="<?php echo esc_url( get_theme_mod( 'clubtravel_twitterlink' ) ); ?>" data-toggle="tooltip" data-placement="<?php if ( is_active_sidebar( 'header-sidebar' ) ) : ?>left<?php else : ?>top<?php endif; ?>" title="<?php esc_html_e( 'Twitter', 'clubtravel' ); ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
	<?php endif; ?>
	<?php if ( get_theme_mod( 'clubtravel_pinterestlink' ) ) : ?>
		<li><a href="<?php echo esc_url( get_theme_mod( 'clubtravel_pinterestlink' ) ); ?>" data-toggle="tooltip" data-placement="<?php if ( is_active_sidebar( 'header-sidebar' ) ) : ?>left<?php else : ?>top<?php endif; ?>" title="<?php esc_html_e( 'Pinterest', 'clubtravel' ); ?>" target="_blank"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
	<?php endif; ?>
	<?php if ( get_theme_mod( 'clubtravel_instagramlink' ) ) : ?>
		<li><a href="<?php echo esc_url( get_theme_mod( 'clubtravel_instagramlink' ) ); ?>" data-toggle="tooltip" data-placement="<?php if ( is_active_sidebar( 'header-sidebar' ) ) : ?>left<?php else : ?>top<?php endif; ?>" title="<?php esc_html_e( 'Instagram', 'clubtravel' ); ?>" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
	<?php endif; ?>
	<?php if ( get_theme_mod( 'clubtravel_linkedinlink' ) ) : ?>
		<li><a href="<?php echo esc_url( get_theme_mod( 'clubtravel_linkedinlink' ) ); ?>" data-toggle="tooltip" data-placement="<?php if ( is_active_sidebar( 'header-sidebar' ) ) : ?>left<?php else : ?>top<?php endif; ?>" title="<?php esc_html_e( 'Linkedin', 'clubtravel' ); ?>" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
	<?php endif; ?>
	<?php if ( get_theme_mod( 'clubtravel_googlepluslink' ) ) : ?>
		<li><a href="<?php echo esc_url( get_theme_mod( 'clubtravel_googlepluslink' ) ); ?>" data-toggle="tooltip" data-placement="<?php if ( is_active_sidebar( 'header-sidebar' ) ) : ?>left<?php else : ?>top<?php endif; ?>" title="<?php esc_html_e( 'Google+', 'clubtravel' ); ?>" target="_blank"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
	<?php endif; ?>
	<?php if ( get_theme_mod( 'clubtravel_youtubelink' ) ) : ?>
		<li><a href="<?php echo esc_url( get_theme_mod( 'clubtravel_youtubelink' ) ); ?>" data-toggle="tooltip" data-placement="<?php if ( is_active_sidebar( 'header-sidebar' ) ) : ?>left<?php else : ?>top<?php endif; ?>" title="<?php esc_html_e( 'YouTube', 'clubtravel' ); ?>" target="_blank"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
	<?php endif; ?>
	<?php if ( get_theme_mod( 'clubtravel_vimeo' ) ) : ?>
		<li><a href="<?php echo esc_url( get_theme_mod( 'clubtravel_vimeo' ) ); ?>" data-toggle="tooltip" data-placement="<?php if ( is_active_sidebar( 'header-sidebar' ) ) : ?>left<?php else : ?>top<?php endif; ?>" title="<?php esc_html_e( 'Vimeo', 'clubtravel' ); ?>" target="_blank"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li>
	<?php endif; ?>
	<?php if ( get_theme_mod( 'clubtravel_tumblrlink' ) ) : ?>
		<li><a href="<?php echo esc_url( get_theme_mod( 'clubtravel_tumblrlink' ) ); ?>" data-toggle="tooltip" data-placement="<?php if ( is_active_sidebar( 'header-sidebar' ) ) : ?>left<?php else : ?>top<?php endif; ?>" title="<?php esc_html_e( 'Tumblr', 'clubtravel' ); ?>" target="_blank"><i class="fa fa-tumblr" aria-hidden="true"></i></a></li>
	<?php endif; ?>
	<?php if ( get_theme_mod( 'clubtravel_flickrlink' ) ) : ?>
		<li><a href="<?php echo esc_url( get_theme_mod( 'clubtravel_flickrlink' ) ); ?>" data-toggle="tooltip" data-placement="<?php if ( is_active_sidebar( 'header-sidebar' ) ) : ?>left<?php else : ?>top<?php endif; ?>" title="<?php esc_html_e( 'Flickr', 'clubtravel' ); ?>" target="_blank"><i class="fa fa-flickr" aria-hidden="true"></i></a></li>
	<?php endif; ?>
	<?php if ( get_theme_mod( 'clubtravel_facebooklink' ) || get_theme_mod( 'clubtravel_twitterlink' ) || get_theme_mod( 'clubtravel_pinterestlink' ) || get_theme_mod( 'clubtravel_instagramlink' ) || get_theme_mod( 'clubtravel_linkedinlink' ) || get_theme_mod( 'clubtravel_googlepluslink' ) || get_theme_mod( 'clubtravel_youtubelink' ) || get_theme_mod( 'clubtravel_vimeo' ) || get_theme_mod( 'clubtravel_tumblrlink' ) || get_theme_mod( 'clubtravel_flickrlink' ) ) : ?>
	</ul>
</div>
<?php endif; ?>
